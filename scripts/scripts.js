/*
 * Oh Hai, enjoy my failscading code.
*/

(function(){
  "use strict";

  var numberOfDice = document.getElementById("numberOfDice");
  var rollBtn = document.getElementById("rollBtn"); 
  var toonInputs = document.getElementsByClassName("toonInputs");
  var addToon = document.getElementById("addToon");
  var rollResult = document.getElementById("rollResult");
  var allInputs = document.getElementsByClassName("allInputs");
  var i = null
/*
 * Define input methods
 * Stop forgetting to put commas between each method.
*/

  var dmTool={
    //Roll the dice
    rollDice : function(){
      var dice = document.getElementById("numberOfDice").value;
      var dSides = document.getElementById("dSides").value;

      switch(dSides){
        case "0":
          var dSides=2;
          break;

        case "1":
          var dSides=3;
          break;

        case "2":
          var dSides=4;
          break;

        case "3":
          var dSides=6;
          break;

        case "4":
          var dSides=8;
          break;

        case "5":
          var dSides=10;
          break;

        case "6":
          var dSides=12;
          break;

        case "7":
          var dSides=20;
          break;

        case "8":
          var dSides=100;
          break;

        default:
          var dSides=0;
          break;
      } //End switch

      if (dice == 1){
      console.log('Rolling', dice, 'die with ', dSides, 'sides selected');
      }
      else{
      console.log('Rolling', dice, 'dice with ', dSides, 'sides selected');
      }
      if (dice <= 100) {
        var dice=Math.round(dice); 
        var result=0;
        var arr = new Array();

        for (i=0; i < dice; i++) {
          var math=Math.round(Math.random() * dSides) % dSides + 1;
          var result= math + result;
          arr.push(math);
        }
        arr.toString();
        rollResult.value=result + " >>> " +  arr;
      } 
      else {
        numberOfDice.value=1;
        rollResult.value="Enter: 0-100";
      }
    },
    //Self explanatory, grabs all the inputs and posts to the table.
    submitToon : function() {
      var toonName = document.getElementById("toonName").value;
      var toonInit = document.getElementById("toonInit").value;
      var toonAc = document.getElementById("toonAc").value;
      var init=Math.round(toonInit * 10) / 10;
      var ac=Math.round(toonAc * 10) / 10;
      var table=document.getElementById("toonTable");
      var row=table.insertRow(1);
      var cell1=row.insertCell(0);
      var cell2=row.insertCell(1);
      var cell3=row.insertCell(2);
      var cell4=row.insertCell(3);
      var cell5=row.insertCell(4);
      if (toonName){
        cell1.innerHTML=toonName ;
      }
      else{
        cell1.innerHTML="Unknown";
      }
      cell2.innerHTML=init;
      cell3.innerHTML=ac;
      // Breaking between input elements broke dom walking due to text nodes being insterted.
      cell4.innerHTML='<INPUT TYPE="number" onkeyup="convertHP(this); ReInit()" \
                        ><INPUT TYPE="number" onkeyup="ReInit()";><INPUT TYPE="text" SIZE="3"\
                        READONLY>';
      cell5.innerHTML='<input type="button" value="Delete" onclick="deleteRow(this)">';
      ReInit();
    },
    addActiveSection: function() {
      this.setAttribute("class", "active");
    },
    removeActiveSection: function () {
      this.removeAttribute("class");
    },
    enterScript: function (evt, script, arg, log) {
      if (evt.keyCode === 13) {
        script(arg);
        if (log){
          console.log(log);
        }
      }
    }

  } // end of object


/*
 * Activate event listeners
*/


  rollBtn.addEventListener("click", dmTool.rollDice, false);
  addToon.addEventListener("click", dmTool.submitToon, false);
  numberOfDice.addEventListener("keypress", function(evt){
    dmTool.enterScript(evt, dmTool.rollDice, null, 'Press "enter" on numberOfDice')
    },false)

  //Add event listener for each member of the class toonInputs.
  for (i=0; i < toonInputs.length; i++){
    toonInputs[i].addEventListener("keypress", function(evt){
      dmTool.enterScript(evt, dmTool.submitToon, null, 'Pressed enter on a toonInput');
        }, false);
  }
  //Add focus/blur to class allInputs
  
  for (i=0; i < allInputs.length; i++){
    allInputs[i].addEventListener("focus", dmTool.addActiveSection, false);
    allInputs[i].addEventListener("blur", dmTool.removeActiveSection, false);
  }
})();

function ReInit(){
  TSort_Data=new Array ('toonTable', 'hs', 'n', 'n');
  tsRegister();
  tsSetTable ('toonTable');
  tsInit();
}

function deleteRow(r){
  var i=r.parentNode.parentNode.rowIndex;
  document.getElementById("toonTable").deleteRow(i);
  ReInit();
}

function convertHP(c){
  var current =c.value;
  var total = c.nextSibling.value;
  var result = c.nextSibling.nextSibling;
  
  result.value = current / total * 100;
  console.log("Current HP of", current, "with a total of", total, "which comes to a percentage of", result.value);   
}
/*
 *********************NOTES*************************
*/

/*
 * I have been going about this all the wrong way.
 * I have been trying to post data to the html table, and then when I'm all done, save it.
 * I get the data in the first place when I'm posting it to the table.
 * Why the fuck don't I just catch the data there?
 *
 * So what I want to do, before the data is posted to HTML, save it to a JSON object.
 * When the user is ready to quit, they click "save" (assuming I want to let them have multiple saves)
 * which will then convert the JSON objects to strings and save them.  On next load, those strings will be 
 * praised back into JSON.  Then a simple for loop should be able to post all the data back into the table.
 *
 * Also keep in mind to delete data from the objects.
*/

/*
 * The row index along with the column index would get me the cell right?
 * This information would allow me to choose which cells to edit and when right?
 * I could have an edit button that once clicked would turn all the cells into inputs right?  With the values they already have?
 * Then I could click another button (or the same button which has changed from 'edit' to 'save' that would save all the changed values right?
 * Or perhaps, I click the edit button, then I can choose which cells to edit by clicking on them, once edit mode is turned off, 
 * then all cells are static values again.  I like this option.
 *
 * Initiative is a hurtle though, I want to be able to sort by init therefore I must have a way to have those numbers as plain text,
 * BUT I must also be able to easily change them... shit.
*/

/*
 * Covert the HTML table to a JSON object as shown on the website.
 * Take the JSON object and convert it to a string.  At this point we can save it to local storage.
 * After that we can load it, convert it back into a JSON object, and then figure out how to 
 * convert the JSON object back into a table.
*/
